#!/bin/sh

wget https://osdn.net/projects/azul/storage/azul-keyring-20230306-1-any.pkg.tar.zst
pacman -U azul-keyring-20230306-1-any.pkg.tar.zst
rm azul-keyring-20230306-1-any.pkg.tar.zst
wget https://osdn.net/projects/azul/storage/azul-mirrors-20230627-1-any.pkg.tar.zst
pacman -U azul-mirrors-20230627-1-any.pkg.tar.zst
rm azul-mirrors-20230627-1-any.pkg.tar.zst
echo " " >> /etc/pacman.conf
echo "[azul]" >> /etc/pacman.conf
echo "Include = /etc/pacman.d/azul-mirrorlist" >> /etc/pacman.conf
pacman -Syy
